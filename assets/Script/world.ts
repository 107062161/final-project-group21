// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property
    gravity_num: number = 0;
    DeBugMode: number = 0;
    Enable: number = 1;

    @property(cc.TiledMap)
    tileMap: cc.TiledMap;

    start() {
        let tiledSize = this.tileMap.getTileSize();
        let layer = this.tileMap.getLayer('road');
        let layerSize = layer.getLayerSize();
        let smogLayer = this.tileMap.getLayer('smog');

        smogLayer.node.active = true;

        for (let i = 0; i < layerSize.width; i++) {
            for (let j = 0; j < layerSize.height; j++) {
                let tiled = smogLayer.getTiledTileAt(i, j, true);
                if (tiled.gid != 0) {
                    tiled.node.group = "smog";
                    let collider = tiled.node.addComponent(cc.BoxCollider);
                    collider.offset = cc.v2(-312, -184);
                    collider.size = tiledSize;
                }
            }
        }
    }

    onLoad() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);

        cc.director.getPhysicsManager().enabled = true;
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2(0, -this.gravity_num);
    }

    onDestroy() {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }

    onKeyDown(e) {
        switch (e.keyCode) {
            case cc.macro.KEY.num1:
                this.DeBugMode = 1;
                break;
        }
    }

    update() {
        if (this.DeBugMode == 1) {
            cc.director.getPhysicsManager().debugDrawFlags = this.Enable;
            cc.director.getCollisionManager().enabledDebugDraw = this.Enable>0? true: false;
            this.Enable = Math.abs(this.Enable - 1);
            this.DeBugMode = 0;
        }
    }

    
}
