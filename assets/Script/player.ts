// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { timeStamp } from "console";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    Bullet: cc.Node;
    @property
    Life: number = 0;
    @property(cc.Node)
    HP: cc.Node;
    @property(cc.Node)
    BT: cc.Node;
    @property(cc.Node)
    AMMO: cc.Node;
    @property(cc.Node)
    SAN: cc.Node;
    @property(cc.Node)
    InHurt: cc.Node;
    @property(cc.AudioClip)
    GunShot_Audio: cc.AudioClip;
    @property(cc.AudioClip)
    Heartbeat_Audio: cc.AudioClip;

    //Key
    Press_A: boolean = false;
    Press_D: boolean = false;
    Press_W: boolean = false;
    Press_K: boolean = false;
    Press_L: boolean = false;
    Press_P: boolean = false;
    //Player Moverment
    onGround: boolean = false;
    OutofBound: boolean = false;
    //anime
    anim: any;
    anim_state: any;
    state: number = 0;
    stand: boolean = false;
    finished: boolean = true;
    OutofBT: boolean = false;
    Hurt: boolean = false;
    HurtEnd: boolean = false;
    //audio
    walk: any;
    Dead: boolean = false;

    onLoad() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.anim = this.node.getComponent(cc.Animation);
        this.anim_state = 'PoliceMan_Idle';
        this.anim.on('finished', this.onAnimaFinished, this);
        cc.audioEngine.setEffectsVolume(0.5);
    }

    onDestroy() {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    onKeyDown(e) {
        switch (e.keyCode) {
            case cc.macro.KEY.a:
                this.Press_A = true;
                break;
            case cc.macro.KEY.d:
                this.Press_D = true;
                break;
            case cc.macro.KEY.space:
                this.Press_W = true;
                break;
            case cc.macro.KEY.k:
                if (this.stand == false && this.finished == true)
                    this.Press_K = true;
                break;
            case cc.macro.KEY.l:
                this.Press_L = true;
                break;
            case cc.macro.KEY.p:
                this.Press_P = true;
                this.HP.width = 0;
                break;
        }
    }

    onKeyUp(e) {
        switch (e.keyCode) {
            case cc.macro.KEY.a:
                this.Press_A = false;
                break;
            case cc.macro.KEY.d:
                this.Press_D = false;
                break;
            case cc.macro.KEY.space:
                this.Press_W = false;
                break;
            case cc.macro.KEY.k:
                this.Press_K = false;
                this.finished = true;
                break;
            case cc.macro.KEY.l:
                this.Press_L = false;
                break;
            case cc.macro.KEY.p:
                this.Press_P = false;
                break;
        }
    }

    start() {

    }

    onAnimaFinished(e, data) {
        if (data.name == 'GunShot') {
            this.stand = false;
            this.node.getChildByName('GunFire').active = false;
            this.AMMO.width -= 10.3;
        }
    }

    setAnim(anim) {
        if (this.anim_state == anim) return;
        this.anim_state = anim;
        this.anim.play(anim);
    }

    //shot
    bullet(dir, x, y) {
        this.Bullet.active = true;
        this.Bullet.x = x + (33 * dir);
        this.Bullet.y = y + 22;
        this.Bullet.runAction(cc.moveBy(0.5, dir * 300, 0));
        setTimeout(() => {
            this.Bullet.active = false;
            this.Bullet.stopAllActions();
        }, 200)
    }
    //state 0: idle (Not wtih Gun)
    //state 1: Gun_idle (With Gun)
    //state 2:...
    update(dt) {
        if(this.Dead == true) {
            this.node.getComponent(cc.PhysicsBoxCollider).size.height = 20;
            //解決無法預測的BUG
            setTimeout(() =>{
                this.node.scaleX = -this.node.scaleX;
                this.node.scaleX = -this.node.scaleX;
            }, 300)
            return;
        }
        let anime = this.anim_state;
        var scaleX = Math.abs(this.node.scaleX);
        var Direction = 0;

        if (this.node.x > 2000 || this.node.y > 1000 || this.node.x < -1000 || this.node.y < -1000) {
            this.node.x = -332.529;
            this.node.y = -221.091;
        }
        //OutofBattery
        if (this.Press_P == true) {
            this.BT.width = 0;
        }
        //Light
        if (this.Press_L == true && this.BT.width > 0) {
            this.node.getChildByName('FlashLight').opacity = 255;
            this.BT.width -= 0.1;
            this.OutofBT = false;
        }
        else if (this.BT.width < 0) {
            this.node.getChildByName('FlashLight').active = false;
            this.OutofBT = true;
        }
        else {
            this.node.getChildByName('FlashLight').opacity = 100;
            this.BT.width -= 0.01;
            this.OutofBT = false;
        }
        //Gunshot
        if (this.Press_K == true && this.state == 1 && this.AMMO.width > 0 &&
            this.finished == true) {
            anime = 'GunShot';
            cc.audioEngine.playEffect(this.GunShot_Audio, false);
            this.finished = false;
            setTimeout(() => {
                this.node.getChildByName('GunFire').active = true;
                this.bullet(this.node.scaleX > 0 ? 1 : -1, this.node.x, this.node.y);
            }, 50)
            this.stand = true;
        }
        //Walk, GunWalk
        if (this.stand == false) {
            if (this.Press_A == true) {
                Direction = -1;
                this.node.scaleX = -scaleX;
                if (this.state == 0) anime = 'WalkFight';
                else if (this.state == 1) anime = 'GunWalk';
            }
            else if (this.Press_D == true) {
                Direction = 1;
                this.node.scaleX = scaleX;
                if (this.state == 0) anime = 'WalkFight';
                else if (this.state == 1) anime = 'GunWalk';
            }
            else {
                anime = 'PoliceMan_Idle'
            }
        }
        //spacebar => jump
        if (this.Press_W == true && this.onGround) {
            this.onGround = false;
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 200);
        }
        //move x axis
        console.log(this.node.getComponent(cc.PhysicsBoxCollider).size.height);
        this.node.x += Direction * 250 * dt;
        if (this.HP.width <= 0) {
            anime = 'Death';
            this.node.getChildByName('FlashLight').active = false;
            this.Dead = true;
        }

        if (anime) {
            this.setAnim(anime);
        }
        if (this.Hurt) {
            if (this.SAN.width > 0) this.SAN.width -= 0.8;
            else if (this.SAN.width <= 0 && this.HP.width > 0) this.HP.width -= 0.1;
            if (this.HurtEnd == false) {
                this.InHurt.active = true;
                this.HurtEnd = true;
            }
        }
        this.Hurt = false;

    }

    onBeginContact(contact, self, other) {
        //Wall Collision
        if (other.node.group == 'Wall') {
            this.onGround = true;
        }
    }

    onCollisionEnter(other, self) {
        //flashlight
        if (other.node.group == 'smog' && self.tag == 1 && this.OutofBT == false) {
            other.node.active = false;
            other.node.getComponent(cc.TiledTile).gid = 0;
        }
        //take gun
        if (other.node.name == 'gun' && self.tag == 2) {
            this.state = 1;
            other.node.active = false;
        }
    }

    onCollisionStay(other, self) {
        if (other.node.group == 'smog' && self.tag == 11 && this.OutofBT == true) {
            this.Hurt = true;
        }
    }

    onCollisionExit(other, self) {
        if (other.node.group == 'smog' && self.tag == 11 && this.OutofBT == true) {
            this.HurtEnd = false;
            this.InHurt.active = false;
        }
    }
}
