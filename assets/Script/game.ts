// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    @property(cc.Node)
    Player_Node:cc.Node;

    start () {

    }

    update (dt) {
        var target_position = this.Player_Node.getPosition();

        var current_position = this.node.getPosition();
        
        current_position.lerp(target_position, 0.1, current_position);

        current_position.y = cc.misc.clampf(target_position.y, 0, 110);

        current_position.x = cc.misc.clampf(target_position.x, 0, 1270);
        this.node.setPosition(current_position);


    }

}
